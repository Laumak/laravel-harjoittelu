var elixir = require('laravel-elixir');

elixir(function(mix) {

    mix.browserSync({
        proxy: 'harj.app'
    });

    mix.sass([
        'app.scss'
    ], 'resources/assets/css');

    mix.styles([
        'app.css',
        'font-awesome.css',
        'summernote.css',
        'select2.min.css'
    ]);

    mix.scripts([
        'jquery-2.1.4.js',
        'bootstrap.js',
        'summernote.js',
        'oma_summernote.js',
        'lang/summernote-fi-FI.js',
        'app.js',
        'select2.min.js'
    ]);
	
});
