<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
	/**
	 * Fillable fields for a tag.
	 * @var array
	 */
	protected $fillable = [
		'name'
	];

	/**
	 * Hae tageihin liittyvät artikkelit.
	 * @return \Illuminate\Database\Eloquent\Relations\BelognsToMany
	 */
    public function articles()
    {
    	return $this->belongsToMany('App\Article');
    }
}
