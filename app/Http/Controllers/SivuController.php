<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;

// use App\Http\Requests;
// use App\Http\Controllers\Controller;

class SivuController extends Controller
{

    /**
     * Etusivu 
     **/
    public function index()
    {
        return view("sivut.etusivu");
    }
    
    /**
     * About sivu
     */
    public function about()
    {
        return view('sivut.about');
    }
}
