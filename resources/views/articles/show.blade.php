@extends('app')

@section('content')
	<h1 class="page-header"> {{ $article->title }} </h1>

	<article>
		{!! $article->body !!}
	</article>

	@unless ($article->tags->isEmpty())
		<h5>Tagit:</h5>
		<ul>
			@foreach ($article->tags as $tag)
				<li>{{ $tag->name }}</li>
			@endforeach
		</ul>
	@endunless

	<hr>

	<a href="{{ url('articles/' . $article->id . '/edit') }}"><button class="btn btn-primary">Muokkaa artikkelia</button></a>
	<a href="{{ url('/articles') }}"><button class="btn btn-primary">Takaisin</button></a>

@stop
