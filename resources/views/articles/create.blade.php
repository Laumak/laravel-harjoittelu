@extends('app')

@section('content')

	<h1>Kirjoita uusi blogikirjoitus</h1>
	<hr>

	@include ('errors.list')

	{!! Form::open(['url' => 'articles']) !!}

		@include ('articles.form', ['submitButtonText' => 'Luo blogikirjoitus'])
		
	{!! Form::close() !!}

@stop
