{{-- Title form group --}}
<div class="form-group">
	{!! Form::label('title', 'Otsikko:') !!}
	{!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

{{-- Body Form Input --}}
<div class="form-group">
	{!! Form::label('body', 'Sisältö:') !!}
	{{ Form::textarea('body', null, ['class' => 'form-control', 'id' => 'summernote']) }}
</div>

{{-- Published at form group --}}
<div class="form-group">
	{!! Form::label('published_at', 'Julkaistaan:') !!}
	{!! Form::input('date', 'published_at', date('Y-m-d'), ['class' => 'form-control']) !!}
</div>

{{-- Tags form group --}}
<div class="form-group">
	{!! Form::label('tag_list', 'Tagit:') !!}
	{!! Form::select('tag_list[]', $tags, null, ['id' => "tag_list", 'class' => 'form-control', 'multiple']) !!}
</div>

<!-- Add Article Form Input -->
<div class="form-group">
	{!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
</div>

@section("footer")
	<script>
	$("#tag_list").select2({
		placeholder: "Valitse tagit"
	});
	</script>
@endsection
