<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Blogi</title>
	<link rel="stylesheet" href="/css/all.css">
	<script src="/js/all.js"></script>

</head>
<body>
	@include("partials.nav")
	<div class="container">

		@include('flash::message')

		@yield("content")

	</div>

	@yield("footer")

</body>
</html>
