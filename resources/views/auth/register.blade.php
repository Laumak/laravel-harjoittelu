<!-- resources/views/auth/register.blade.php -->

@extends('app')

@section('content')

    <h1>Rekisteröidy käyttjäksi</h1>
    <hr>

    @include ('errors.list')

    {!! Form::open(['url' => '/auth/register']) !!}
        {!! csrf_field() !!}

        {{-- Name form group --}}
        <div class="form-group">
            {!! Form::label('title', 'Nimi:') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>

        {{-- Email form group --}}
        <div class="form-group">
            {!! Form::label('title', 'Sähköpostiosoite:') !!}
            {!! Form::email('email', null, ['class' => 'form-control']) !!}
        </div>

        {{-- Password Form Input --}}
        <div class="form-group">
            {!! Form::label('title', 'Salasana:') !!}
            {!! Form::password('password', ['class' => 'form-control']) !!}
        </div>

        {{-- Password Confirmation at form group --}}
        <div class="form-group">
            {!! Form::label('title', 'Varmista salasana:') !!}
            {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
        </div>

        <!-- Add Article Form Input -->
        <div class="form-group">
            {!! Form::submit('Rekisteröidy', ['class' => 'btn btn-primary form-control']) !!}
        </div>

    {!! Form::close() !!}

@stop
