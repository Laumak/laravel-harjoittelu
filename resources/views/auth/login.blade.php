<!-- resources/views/auth/login.blade.php -->

@extends('app')

@section('content')

    <h1>Kirjaudu</h1>
    <hr>

    @include ('errors.list')

    {!! Form::open(['url' => 'auth/login']) !!}
        {!! csrf_field() !!}

        {{-- Email form group --}}
        <div class="form-group">
            {!! Form::label('title', 'Sähköpostiosoite:') !!}
            {!! Form::email('email', null, ['class' => 'form-control']) !!}
        </div>

        {{-- Password Form Input --}}
        <div class="form-group">
            {!! Form::label('title', 'Salasana:') !!}
            {!! Form::password('password', ['class' => 'form-control']) !!}
        </div>

        {{-- Checkbox at form group --}}
        <div class="form-group">
            {!! Form::label('title', 'Muista kirjautuminen:') !!}
            {!! Form::checkbox('remember', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Add Article Form Input -->
        <div class="form-group">
            {!! Form::submit('Kirjaudu', ['class' => 'btn btn-primary form-control']) !!}
        </div>

    {!! Form::close() !!}

@stop
